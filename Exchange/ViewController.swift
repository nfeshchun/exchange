//
//  ViewController.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Mark IBOutlet
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var exchangeButton: UIButton!
    @IBOutlet weak var valueInput: UITextField!
    @IBOutlet weak var currencyTransferedSV: CurrencySlider!
    @IBOutlet weak var currencyTransferSV: CurrencySlider!
    @IBOutlet weak var transferPages: UIPageControl!
    @IBOutlet weak var transferedPages: UIPageControl!
    
    //Mark services
    lazy var networkService: INetworkService = NetworkService()
    lazy var currencyHolder:CurrencyHolder = CurrencyHolder()
    
    //Mark properties
    private var timer:NSTimer? = nil;
    private var repeatTime:Double = 30.0; // seconds
    
    private let currenciesList = [CurrencyList.USD,CurrencyList.GBP,CurrencyList.EUR]
    private let message = "Not enough money for conversion"
    
    //Mark Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        populateMoneyBag()
        populateSlider(currencyTransferSV)
        populateSlider(currencyTransferedSV)
        startUpdateCurrency()
        addScrollListener()
        setupCurrencies()
        
        valueInput.addTarget(self, action: #selector(self.moneyInputListener(_:)), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        valueInput.becomeFirstResponder()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark IBActions
    
    @IBAction func cancel(sender: AnyObject) {
    }
    
    @IBAction func exchange(sender: AnyObject) {
        if valueInput.text == "" || currenciesList[transferPages.currentPage] == currenciesList[transferedPages.currentPage] {
            
            return
        }
        if !currencyHolder.exchange() {
            presentAlertController(message)
        }
    }
    

    //Mark methods
    
    func moneyInputListener(textField: UITextField) {
        if textField.text != "" {
            let amount = Double(textField.text!)!
            if currencyHolder.enoughtMoneyConversion(amount) {
                currencyHolder.exchangeAmount(Double(textField.text!)!)
            } else {
                presentAlertController(message)
            }
        } else {
            currencyHolder.exchangeAmount(0)
        }
    }
    
    func populateMoneyBag() {
        for currencyType in CurrencyList.allValues {
            currencyHolder.populateMoneyBag(currencyType, value: 100.0)
        }
    }
    
    func setupCurrencies() {
        if let views = currencyTransferSV.childs() {
            for index in 0...views.count - 1 {
                let currencyView = views[index]  as! CurrencyView
                currencyView.selectCurrency(currenciesList[index])
                
            }
        }
        
        if let views = currencyTransferedSV.childs() {
            for index in 0...views.count - 1 {
                let currencyView = views[index]  as! CurrencyView
                currencyView.selectCurrency(currenciesList[index])
                currencyView.transfered(true)
            }
        }
    }
    
    func addScrollListener() {
        currencyTransferSV.addChangeListener { (pageNumber) in
            self.transferPages.currentPage = pageNumber
            self.currencyHolder.firstCurrency(self.currenciesList[self.transferPages.currentPage])
            
        }
        currencyTransferedSV.addChangeListener { (pageNumber) in
            self.transferedPages.currentPage = pageNumber
            self.currencyHolder.secondCurrency(self.currenciesList[self.transferedPages.currentPage])
        }
    }
    
    func populateSlider(slider: CurrencySlider) {
        //1
        var views:Array<CurrencyView> = []
        
        for index in 0...CurrencyList.count - 1 {
            let view = CurrencyView.createFromNib() as! CurrencyView
            currencyHolder.addRateListener({ 
                view.updateUI()
            })
            view.injectMoneyBag(currencyHolder)
            views.append(view)
            view.tag = index
        }
        
        // 2
        slider.numPages = CurrencyList.count
        slider.viewObjects = views
        
        // 3
        slider.setup()
    }
    
    func startUpdateCurrency() {
        timer = NSTimer.scheduledTimerWithTimeInterval(repeatTime,
                                                       target: self,
                                                       selector: #selector(ViewController.loadData),
                                                       userInfo: nil,
                                                       repeats: true)
        timer?.fire()
    }
    func stopUpdateCurrency() {
        timer?.invalidate()
        timer = nil
    }
    
    func loadData() {
        networkService.loadData({ (currenciesNetwork) in
            if currenciesNetwork != nil {
                self.currencyHolder.setupRates(currenciesNetwork!)
            }
        })
    }
    
    func presentAlertController(string: String) {
        let alertController = UIAlertController(title: "Exchange", message: string
            , preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}

