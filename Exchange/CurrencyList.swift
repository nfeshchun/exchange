//
//  CurrencyList.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation

public enum CurrencyList : Int {
    
    case USD = 0
    case GBP = 1
    case EUR = 2
    
    var description: String
    {
        switch self
        {
        case .USD:		return "USD"
        case .GBP:		return "GBP"
        case .EUR:		return "EUR"
        }
    }
    
    var currencySign: String
    {
        switch self
        {
        case .USD:		return "$"
        case .GBP:		return "£"
        case .EUR:		return "€"
        }
    }
    
    static func stringToValue(string: String)-> CurrencyList? {
        switch string
        {
        case "USD":		return .USD
        case "GBP":		return .GBP
        case "EUR":		return .EUR
        default:
            return nil
        }
    }
    
    static var count: Int { return CurrencyList.EUR.rawValue + 1}
    static let allValues = [USD, GBP, EUR]
}
