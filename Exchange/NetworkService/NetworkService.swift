//
//  NetworkService.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation
import AEXML

class NetworkService: INetworkService {
    let dayTimePeriodFormatter = NSDateFormatter()
    
    let baseURL = NSURL(string: "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")
    
    let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    
    
    func loadData(completion: (currencies: Dictionary<CurrencyList, Double>?) -> Void) {
        var dataTask:NSURLSessionDataTask? = nil;
        
        dataTask = session.dataTaskWithURL(baseURL!, completionHandler: { (data, response, error) in
            
            if let dictionary = self.parseXml(data!) {
                completion(currencies: dictionary)
            }
        })
        dataTask?.resume()
    }
    
    func parseXml(data: NSData)-> Dictionary<CurrencyList, Double>? {
        do {
            let xmlDoc = try AEXMLDocument(xmlData: data)
            
            // prints the same XML structure as original
            var itemsDictionary: Dictionary<CurrencyList,Double> = [:]
            let rateUSD = xmlDoc.root["Cube"]["Cube"]["Cube"].allWithAttributes(["currency":"USD"])![0].attributes["rate"]
            let rateGBP = xmlDoc.root["Cube"]["Cube"]["Cube"].allWithAttributes(["currency":"GBP"])![0].attributes["rate"]
            
            itemsDictionary[CurrencyList.EUR] = 1.0
            itemsDictionary[CurrencyList.USD] = Double(rateUSD!)
            itemsDictionary[CurrencyList.GBP] = Double(rateGBP!)
            return itemsDictionary
        } catch {
            
        }
        return nil
    }
}