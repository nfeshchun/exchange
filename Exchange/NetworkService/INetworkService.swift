//
//  INetworkService.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation

public protocol INetworkService : class {
    func loadData(completion: (currencies: Dictionary<CurrencyList, Double>?)->Void)
}