//
//  Double+Print.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation
extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}