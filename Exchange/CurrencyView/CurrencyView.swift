//
//  CurrencyView.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation
import UIKit

class CurrencyView: UIView {
    
    //Mark IBOutlet
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var hintRateLabel: UILabel!
    
    //Mark properties
    private var selectedCurrency: CurrencyList = CurrencyList.USD
    private let hintString = "You have "
    private weak var moneyBag: IMoneyBag!
    
    private var isTransfered: Bool = false {
        didSet {
            updateUI()
        }
    }
    
    //Mark create
    class func createFromNib() -> UIView {
        return UINib(nibName: "CurrencyView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! CurrencyView
    }
    
    //Mark methods
    func selectCurrency(currency: CurrencyList) {
        selectedCurrency = currency
        currencyLabel.text = selectedCurrency.description
    }
    
    func updateUI() {
        hintLabel.text = hintString + selectedCurrency.currencySign + "\(moneyBag!.retrieveMoney(selectedCurrency).format(".2"))"
        
        hintRateLabel.hidden = !isTransfered
        if !moneyBag.possibleToExchange() {
            valueLabel.text = "0"
            hintRateLabel.text = ""
            return
        }
        if isTransfered {
            if moneyBag.recieve() == 0 {
                valueLabel.text = "0"
            } else {
                valueLabel.text = "+ \(moneyBag.recieve().format(".2"))"
            }
            hintRateLabel.text = selectedCurrency.currencySign + "1 = \(moneyBag.exchangeRate().format(".2"))"
        } else {
            if moneyBag.loose() == 0 {
                valueLabel.text = "0"
            } else {
                valueLabel.text = "- \(moneyBag.loose().format(".2"))"
            }
        }
    }
    
    func currentCurrency() -> CurrencyList {
        return selectedCurrency
    }
    
    func injectMoneyBag(moneyBag: CurrencyHolder) {
        self.moneyBag = moneyBag
    }
    
    func transfered(isTransfered: Bool) {
        self.isTransfered = isTransfered
    }
    
}