//
//  ICurrencySlider.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation
import UIKit

public typealias ChangeListener = ((pageNumber: Int)-> Void);

protocol ICurrencySlider: class {
    
    func addChangeListener(changeListener: ChangeListener)
    func childs()-> [UIView]?
}