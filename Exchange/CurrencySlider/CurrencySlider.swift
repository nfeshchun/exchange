//
//  CurrencySlider.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation
import UIKit

class CurrencySlider : UIScrollView, UIScrollViewDelegate, ICurrencySlider {
    
    // 1
    var viewObjects: [CurrencyView]?
    var numPages: Int = 0
    var changeListener: ChangeListener?
    var currentIndex = 0
    // 2
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        pagingEnabled = true
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        scrollsToTop = false
        delegate = self
    }
    
    // 3
    func setup() {
        guard superview != nil else { return }
        
        contentSize = CGSize(width: (frame.size.width * (CGFloat(numPages) + 2)), height: frame.size.height)
        
        loadScrollViewWithPage(0)
        loadScrollViewWithPage(1)
        loadScrollViewWithPage(2)
        
        var newFrame = frame
        newFrame.origin.x = newFrame.size.width
        newFrame.origin.y = 0
        scrollRectToVisible(newFrame, animated: false)
//        layoutIfNeeded()
    }
    
    // 4
    private func loadScrollViewWithPage(page: Int) {
        if page < 0 { return }
        if page >= numPages + 2 { return }
        
        var index = 0
        
        if page == 0 {
            index = numPages - 1
        } else if page == numPages + 1 {
            index = 0
        } else {
            index = page - 1
        }
        
        let view = viewObjects?[index]
        
        var newFrame = frame
        newFrame.origin.x = frame.size.width * CGFloat(page)
        newFrame.origin.y = 0
        view?.frame = newFrame
        
        if view?.superview == nil {
            addSubview(view!)
        }
        
//        changeListener?(pageNumber: Int(index))
        
        layoutIfNeeded()
    }
    
    // 5
    @objc internal func scrollViewDidScroll(scrollView: UIScrollView) {
        let pageWidth = frame.size.width
        let page = floor((contentOffset.x - (pageWidth/2)) / pageWidth) + 1
        
        loadScrollViewWithPage(Int(page - 1))
        loadScrollViewWithPage(Int(page))
        loadScrollViewWithPage(Int(page + 1))
    }
    
    // 6
    @objc internal func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageWidth = frame.size.width
        let page : Int = Int(floor((contentOffset.x - (pageWidth/2)) / pageWidth) + 1)
        
        if page == 0 {
            contentOffset = CGPoint(x: pageWidth*(CGFloat(numPages)), y: 0)
        } else if page == numPages+1 {
            contentOffset = CGPoint(x: pageWidth, y: 0)
        }
        var index = 0
        
        if page == 0 {
            index = numPages - 1
        } else if page == numPages + 1 {
            index = 0
        } else {
            index = page - 1
        }
        
        changeListener?(pageNumber: Int(index))

        
    }
    //7
    func addChangeListener(changeListener: ChangeListener) {
        self.changeListener = changeListener
    }
    
    func childs() -> [UIView]? {
        return viewObjects
    }
    
    
}