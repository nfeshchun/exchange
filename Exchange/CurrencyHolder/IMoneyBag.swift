//
//  IMoneyBag.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation

protocol IMoneyBag: class {
    func retrieveMoney(key: CurrencyList)-> Double
    func rateForCurrency(key: CurrencyList)-> Double
    func exchangeRate() -> Double
    func loose()-> Double
    func recieve()-> Double
    func possibleToExchange()->Bool
}