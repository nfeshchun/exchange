//
//  CurrencyHolder.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation

class CurrencyHolder: IMoneyBag, IExchange {
    
    //Mark properties
    
    private var moneyBag: Dictionary<CurrencyList,Double> = [:]
    private var exchangeRates: Dictionary<CurrencyList,Double> = [:]
    private var listeners: [RatesListener] = []
    private var firstCurrency: CurrencyList = CurrencyList.USD
    private var secondCurrency: CurrencyList =  CurrencyList.USD
    private var amount: Double =  0.0
    var convertedEuro: Double = 0.0
    var convertedResult: Double = 0.0
    
    //Mark methods
    
    func populateMoneyBag(key: CurrencyList, value: Double) {
        moneyBag[key] = value
    }
    
    func retrieveMoney(key: CurrencyList)-> Double {
        return moneyBag[key]!
    }
    
    func rateForCurrency(key: CurrencyList)-> Double {
        if exchangeRates.count == 0 {
            return 0
        }
        return exchangeRates[key]!
    }
    
    func setupRates(rates: Dictionary<CurrencyList,Double>) {
        exchangeRates = rates
        updateListeners()
    }
    
    func addRateListener(listener: RatesListener) {
        listeners.append(listener)
    }
    
    func updateListeners() {
        for listener in listeners {
            listener()
        }
    }
    
    func firstCurrency(firstCurrency: CurrencyList) {
        self.firstCurrency = firstCurrency
        updateListeners()
    }
    
    func secondCurrency(secondCurrency: CurrencyList) {
        self.secondCurrency = secondCurrency
        updateListeners()
    }
    
    func exchangeAmount(amount: Double) {
        self.amount = amount
        updateListeners()
    }
    
    func exchangeRate() -> Double {
        if exchangeRates.count > 0 {
            let convertedEuro = 1 / exchangeRates[firstCurrency]!
            let convertedResult = convertedEuro * exchangeRates[secondCurrency]!
            return convertedResult
        }
        return 1.0
    }
    
    func exchange()-> Bool {
        
        let totalMoney = retrieveMoney(firstCurrency)
        let totalConvertedMoney = retrieveMoney(secondCurrency)
        
        if totalMoney < amount {
            return false
        }
        
        moneyBag[firstCurrency] = totalMoney - amount
        moneyBag[secondCurrency] = totalConvertedMoney + convertedResult
        
        updateListeners()
        return true
    }
    
    func loose() -> Double {
        return amount
    }
    
    func recieve() -> Double {
        if exchangeRates.count == 0 {
            return 0
        }
        convertedEuro = amount / exchangeRates[firstCurrency]!
        convertedResult = convertedEuro * exchangeRates[secondCurrency]!
        return convertedResult
    }
    
    func possibleToExchange() -> Bool {
        return firstCurrency != secondCurrency
    }
    
    func enoughtMoneyConversion(amount: Double) -> Bool {
        return amount < moneyBag[firstCurrency]
    }
    
}