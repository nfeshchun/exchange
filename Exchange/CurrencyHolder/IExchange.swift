//
//  IExchange.swift
//  Exchange
//
//  Created by Nikita on 24.07.16.
//  Copyright © 2016 Nikita. All rights reserved.
//

import Foundation

public typealias RatesListener = ()->Void;

protocol IExchange: class {
    func addRateListener(listener: RatesListener)
    func updateListeners()
    func firstCurrency(firstCurrency: CurrencyList)
    func secondCurrency(secondCurrency: CurrencyList)
    func exchange()-> Bool
    func exchangeAmount(amount: Double)
    func enoughtMoneyConversion(amount: Double)-> Bool
}